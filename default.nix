{ pkgs ? import <nixpkgs> {} }:
  pkgs.mkShell {
    nativeBuildInputs = with pkgs.buildPackages; [
      openssl
      pkg-config
      libxml2
      glib
      sqlite
    ];
}
