use super::{Category, CategoryMapping, Enclosure, FatArticle, Feed, FeedMapping, Headline, Tagging};

pub struct StreamConversionResult {
    pub articles: Vec<FatArticle>,
    pub headlines: Vec<Headline>,
    pub taggings: Vec<Tagging>,
    pub enclosures: Vec<Enclosure>,
}

pub struct FeedConversionResult {
    pub feeds: Vec<Feed>,
    pub feed_mappings: Vec<FeedMapping>,
    pub categories: Vec<Category>,
    pub category_mappings: Vec<CategoryMapping>,
}
