use crate::models::{Article, ArticleID};
use crate::models::{Marked, Read};
use crate::schema::articles;

#[derive(Identifiable, Queryable, Eq, PartialEq, Debug)]
#[diesel(primary_key(article_id))]
#[diesel(table_name = articles)]
pub struct Headline {
    pub article_id: ArticleID,
    pub unread: Read,
    pub marked: Marked,
}

impl Headline {
    pub fn from_article(article: &Article) -> Headline {
        Headline {
            article_id: article.article_id.clone(),
            unread: article.unread,
            marked: article.marked,
        }
    }
}
