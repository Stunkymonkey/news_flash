pub mod metadata;

use self::metadata::TemplateMetadata;
use crate::feed_api::{FeedApi, FeedApiErrorKind, FeedApiResult, Portal};
use crate::models::{
    self, ArticleID, Category, CategoryID, FatArticle, FavIcon, Feed, FeedID, LoginData, Marked, PluginCapabilities, Read, SyncResult, TagID, Url,
};
use async_trait::async_trait;
use chrono::{DateTime, Utc};
use failure::ResultExt;
use log::{error, warn};
use reqwest::Client;

pub struct TemplateService {
    portal: Box<dyn Portal>,
}

#[async_trait]
impl FeedApi for TemplateService {
    fn features(&self) -> FeedApiResult<PluginCapabilities> {
        Ok(PluginCapabilities::NONE)
    }

    fn has_user_configured(&self) -> FeedApiResult<bool> {
        unimplemented!()
    }

    async fn is_logged_in(&self, _client: &Client) -> FeedApiResult<bool> {
        unimplemented!()
    }

    fn user_name(&self) -> Option<String> {
        unimplemented!()
    }

    fn get_login_data(&self) -> Option<LoginData> {
        unimplemented!()
    }

    fn get_api_secret(&self) -> Option<ApiSecret> {
        unimplemented!()
    }

    async fn login(&mut self, _data: LoginData, _client: &Client) -> FeedApiResult<()> {
        unimplemented!()
    }

    async fn logout(&mut self, _client: &Client) -> FeedApiResult<()> {
        unimplemented!()
    }

    async fn initial_sync(&self, client: &Client) -> FeedApiResult<SyncResult> {
    	unimplemented!()
    }

    async fn sync(&self, _max_count: u32, _last_sync: DateTime<Utc>, client: &Client) -> FeedApiResult<SyncResult> {
        unimplemented!()
    }

    async fn set_article_read(&self, _articles: &[ArticleID], _read: models::Read, _client: &Client) -> FeedApiResult<()> {
        unimplemented!()
    }

    async fn set_article_marked(&self, _articles: &[ArticleID], _marked: models::Marked, _client: &Client) -> FeedApiResult<()> {
        unimplemented!()
    }

    async fn set_feed_read(&self, _feeds: &[FeedID], _last_sync: DateTime<Utc>, _client: &Client) -> FeedApiResult<()> {
        unimplemented!()
    }

    async fn set_category_read(&self, _categories: &[CategoryID], _last_sync: DateTime<Utc>, _client: &Client) -> FeedApiResult<()> {
        unimplemented!()
    }

    async fn set_tag_read(&self, _tags: &[TagID], _last_sync: DateTime<Utc>, _client: &Client) -> FeedApiResult<()> {
        unimplemented!()
    }

    async fn set_all_read(&self, _last_sync: DateTime<Utc>, _client: &Client) -> FeedApiResult<()> {
        unimplemented!()
    }

    async fn add_feed(
        &self,
        url: &Url,
        title: Option<String>,
        category_id: Option<CategoryID>,
        client: &Client,
    ) -> FeedApiResult<(Feed, Option<Category>)> {
    	unimplemented!()
    }

    async fn remove_feed(&self, _id: &FeedID, _client: &Client) -> FeedApiResult<()> {
        unimplemented!()
    }

    async fn move_feed(&self, _feed_id: &FeedID, _from: &CategoryID, _to: &CategoryID, _client: &Client) -> FeedApiResult<()> {
        unimplemented!()
    }

    async fn rename_feed(&self, feed_id: &FeedID, _new_title: &str, _client: &Client) -> FeedApiResult<FeedID> {
        unimplemented!()
    }

    async fn add_category(&self, title: &str, _parent: Option<&CategoryID>, _client: &Client) -> FeedApiResult<CategoryID> {
        unimplemented!()
    }

    async fn remove_category(&self, _id: &CategoryID, _remove_children: bool, _client: &Client) -> FeedApiResult<()> {
        unimplemented!()
    }

    async fn rename_category(&self, id: &CategoryID, _new_title: &str, _client: &Client) -> FeedApiResult<CategoryID> {
        unimplemented!()
    }

    async fn move_category(&self, _id: &CategoryID, _parent: &CategoryID, _client: &Client) -> FeedApiResult<()> {
        unimplemented!()
    }

    async fn import_opml(&self, _opml: &str, _client: &Client) -> FeedApiResult<()> {
        unimplemented!()
    }

    async fn add_tag(&self, title: &str, _client: &Client) -> FeedApiResult<TagID> {
        unimplemented!()
    }

    async fn remove_tag(&self, _id: &TagID, _client: &Client) -> FeedApiResult<()> {
        unimplemented!()
    }

    async fn rename_tag(&self, id: &TagID, _new_title: &str, _client: &Client) -> FeedApiResult<TagID> {
        unimplemented!()
    }

    async fn tag_article(&self, _article_id: &ArticleID, _tag_id: &TagID, _client: &Client) -> FeedApiResult<()> {
        unimplemented!()
    }

    async fn untag_article(&self, _article_id: &ArticleID, _tag_id: &TagID, _client: &Client) -> FeedApiResult<()> {
        unimplemented!()
    }

    async fn get_favicon(&self, _feed_id: &FeedID, _client: &Client) -> FeedApiResult<FavIcon> {
        unimplemented!()
    }
}
